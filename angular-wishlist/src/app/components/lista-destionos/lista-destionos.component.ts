import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destionos',
  templateUrl: './lista-destionos.component.html',
  styleUrls: ['./lista-destionos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestionosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all: DestinoViaje[];

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>){
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if( d != null ){
          this.updates.push("Se ha elegido a " + d.nombre);
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // this.store.dispatch(new NuevoDestinoAction(d));
    // console.log(this.destinos);

  }
  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
    // this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }
}

